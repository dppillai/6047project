# README: Dynamic Network Analysis in Host Response to Infectious Disease #
This repository was created for a 6.047/ 6.878 class project by Divya Pillai (dppillai[at]mit.edu) and Kris Shin (shinh[at]mit.edu). We aim to describe the host response to viral infection as it varies with time post-infection. We are currently working with data from the Influenza research database. 

### TODO ###
* HMMs: auto-run til convergence. Fix convergence condition to be more strict (but be careful since this can take forever). Figure out ways to speed up?
** Consider implementing Baum-Welch but beware of storage/ time costs. 
* Evaluate performance, potentially involving MI- mutual information. 

### Repository structure ###
There are two directories: data, which stores source data files in excel format and other relevant info, and code, which stores the python code run to give our results.

* main.py: "runner" file, integrates all steps elsewhere
* parsedata.py: functions for reading data from source excel files. 
* hmm.py: hidden markov model adapted for this project, with (most of) Viterbi-based unsupervised learning implemented. Graph state is represented as an adjacency list matrix.
* stringnetworks.py: code to query STRING API for network information, or just for resolved gene/ protein IDs, stored in geneids. 
* ppigraph.py: graphing functions with networkx and matplotlib
* evalroc.py: calculates confusion matrix between HMM (observed positive/ negative) and STRING ("true" positive/ negative) interactors
* geneids: a python variable storing a dictionary of resolved gene/ protein IDs obtained from STRING (hashes public ids in weird formats to standard Ensembl formats)

### Who do I talk to? ###

* Divya Pillai (dppillai)
* Kris (Hakyung) Shin (shinh)