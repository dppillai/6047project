import stringnetworks

# NOTE: parameter interactors only for one time
def confusion_matrix(interactors1time, iddict):
	tottp = 0
	totfp = 0
	totfn = 0
	tottn = 0

	N = len(interactors1time)
	norm = 1.0/(N*N)
	for prot in interactors1time: 
		tp=0
		fp=0
		fn=0
		# observed
		hmm_interactors = list(set([iddict[x] for x in interactors1time[prot]]))
		# "true"
		str_interactors = stringnetworks.get_interactor_names(prot,score=001)

		for p in hmm_interactors:
			if p in str_interactors:
				tp+=1
			else:
				fp+=1
		for p in str_interactors:
			if p not in hmm_interactors:
				fn +=1
		tn=N-1-tp-fp-fn #? something like that

		tottp += tp*norm
		totfp += fp*norm
		totfn += fn*norm
		tottn += tn*norm

	return  [[tottp, totfp],[totfn, tottn]]
