from stringnetworks import load_ids
import math

def states_from_ids(iddict):
	adjacency = {}
	pids = sorted(iddict.keys()) #use sorted nature of pids later
	for i in range(len(pids)):
		curprot = pids[i]
		adjacency[curprot] = {}
		for j in range(i+1,len(pids)):
			pairprot = pids[j]
			adjacency[curprot][pairprot] = 0
	return adjacency

def adj_matrix_from_ids(iddict):
	pids = sorted([x for x in iddict if len(iddict[x])>0])
	adjmat = [[0]*len(pids)]* (len(pids))
	return (adjmat, pids)

def find_prots_in_adj(pids, prot1, prot2):
	a = pids.index(prot1)
	b = pids.index(prot2)
	return (min(a,b),max(a,b))

def normalize_rows(matrix):
	for i in range(len(matrix)):
		rowsum = sum(matrix[i])
		matrix[i] = [matrix[i][j]*1.0/rowsum for j in range(len(matrix[i]))]
	return matrix

#precondition: mat1 and mat2 have same size
def converged(mat1,mat2):
	diffs = [[abs(mat1[i][j]-mat2[i][j]) for j in range(len(mat1[0]))] for i in range(len(mat1))]
	return ((sum([sum(x) for x in diffs])/(len(mat1)*len(mat1[0]))) < .1)

def run_til_convergence_or_maxruns(maxruns,obsdata):
	myhmm = HMM(O=obsdata)
	print 'made hmm'
	prevemit = myhmm.emission
	prevtrans = myhmm.transition
	(prob, emit, trans, interactors) = myhmm.viterbi_iterate()
	print 'Emission: '+str(emit)
	print 'Transition: '+str(trans)
	print 'Emission matrix converged:'+str(converged(prevemit, emit))
	print 'Transition matrix converged: '+str(converged(prevtrans, trans))
	for run in range(maxruns):
		print 'Iteration '+str(run+1)
		# reiterate with reset params
		myhmm.reset_params(emit, trans)	
		prevemit = emit
		prevtrans = trans
		(prob, emit, trans, interactors) = myhmm.viterbi_iterate()
		print 'Emission: '+str(emit)
		print 'Transition: '+str(trans)
		econverge = converged(prevemit, emit)
		tconverge = converged(prevtrans, trans)
		print 'Emission matrix converged:'+str(econverge)
		print 'Transition matrix converged: '+str(tconverge)
		if econverge & tconverge:
			return (myhmm, interactors)
	return (myhmm, interactors)

#draft HMM class, TODO document
class HMM:

	#for now, the probability matrices will be the same for any pair of proteins P1 and P2, in the default implementation 
	def __init__(self, E=[[.5, .48, .02],[.01, .06, .93]],T=[[.6, .4],[.6, .4]], O={}):
		iddict = load_ids()
		(a,p) = adj_matrix_from_ids(iddict)
		self.startstatemat = a
		self.pids = p
		#Numbers made up (best guess, acceptable for Viterbi approach). 
		#if interaction: 93% likely to express both p1 and p2?, 3% to express either alone, 1% to express none. 
		#if no interaction, any chance of expressing p1/p2/none, lower for both
		self.start = [0.5,0.5] #technically no interaction, but allow equal prob of having interaction
		self.emission = E 
		self.transition = T	
		self.observations = O# time to data

	def reset_params(self, E,T): # the only params that need to be reset after iterations?
		self.emission = E
		self.transition = T

	def v_prob(self,prevstate, curemit):
		#prevstate= 0 for no interaction, 1 for interaction
		#curemit= 0 none, 1 for single prot, 2 for both
		prob0 = math.log(self.transition[prevstate][0])+math.log(self.emission[0][curemit])
		prob1 = math.log(self.transition[prevstate][1])+math.log(self.emission[1][curemit])
		if prob0 > prob1:
			return (0,prob0)
		return (1,prob1)

	def next_viterbi_state_mat(self,prevstatemat,curemitted):
		totprob = 0
		newstatemat = [[0]*len(self.pids)]* (len(self.pids))
		for i in range(len(newstatemat)):
			protemit = 0
			if self.pids[i] in curemitted:
				protemit = 1
			for j in range(i+1,len(newstatemat)):
				prevstate = prevstatemat[i][j]
				numemissions = protemit
				if self.pids[j] in curemitted:
					numemissions +=1 
				# get new Viterbi state and prob based on prevstate and curemission
				(newstate, prob) = self.v_prob(prevstate,numemissions)
				# newstate = 1 for interaction, 0 for no interaction
				newstatemat[i][j] = newstate
				totprob+=prob
		return (newstatemat, totprob)	

	# update emission and transition prob matrices
	def counts_from_states(self, prevstatemat, newstatemat,curobs):
		# times = sorted(self.states.keys())	
		emissions = [[0,0,0],[0,0,0]]
		transitions = [[0,0],[0,0]]

		for i in range(len(prevstatemat)):
			for j in range(i+1,len(prevstatemat)):
				curstate = newstatemat[i][j]
				# updating transitions
				prevstate = prevstatemat[i][j]
				transitions[prevstate][curstate] += 1
				# updating emissions
				numprots = 0
				if self.pids[i] in curobs:
					numprots +=1
				if self.pids[j] in curobs:
					numprots +=1
				emissions[curstate][numprots] += 1

		return (emissions, transitions)

	def viterbi_iterate(self):
		times = sorted(self.observations.keys())
		totalprob = 0
		interactors = {}
		# these matrices are doubling as the pseudocounts- not sure what else to use. should be relatively very small. 
		# making these counts a bit more uniform so initial guesses don't bias
		emission = [[.4, .4, .2],[.1, .2, .7]] 
		emission = [[emission[i][j] for j in range(len(emission[0]))] for i in range(len(emission))]
		transition = [[.6, .4],[.5, .5]]
		transition = [[transition[i][j] for j in range(len(transition[0]))] for i in range(len(transition))]
		# initial state of all 0s remains in self.startstatemat; others stored in temporary variables
		prevstatemat = self.startstatemat
		for i in range(len(times)):
			curobs = self.observations[times[i]]
			(newstatemat,prob) = self.next_viterbi_state_mat(prevstatemat,curobs)
			totalprob += prob
			interactors[times[i]] = self.interactors_from_state(newstatemat)
			(emitcounts, transcounts) = self.counts_from_states(prevstatemat, newstatemat, curobs)
			emission = [[emission[i][j]+ emitcounts[i][j] for j in range(len(emission[0]))] for i in range(len(emission))]
			transition = [[transition[i][j]+ transcounts[i][j] for j in range(len(transition[0]))] for i in range(len(transition))]
			prevstatemat = newstatemat

		#converting pseudocounts to parameters by normalizing rows
		emission = normalize_rows(emission)
		transition = normalize_rows(transition)

		return (totalprob,emission,transition, interactors)

	def add_observations(self,time,expr_proteins):
		if time in self.observations:
			# do we want to do anything?
			for prot in expr_proteins:
				if prot not in self.observations[time]:
					self.observations[time].append(prot)
		else:
			self.observations[time]=expr_proteins

	def interactors_from_state(self,statemat):
		interactors = {}
		for i in range(len(statemat)):
			prot1 = self.pids[i]
			for j in range(i+1,len(statemat)):
				prot2 = self.pids[j]
				if statemat[i][j] == 1 :
					if prot1 not in interactors:
						interactors[prot1] = set()
					interactors[prot1].add(prot2)
					if prot2 not in interactors:
						interactors[prot2] = set()
					interactors[prot2].add(prot1)
		return interactors

#if interaction: 93% likely to express both p1 and p2?, 3% to express either alone, 1% to express none. 
#if no interaction, any chance of expressing p1/p2/none, lower for both
