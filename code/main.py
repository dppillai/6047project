import os
import parsedata
import stringnetworks

# All excel source data files are stored in data folder, with given filenames, for a given time in hours
timefiles = {7: 'results1time7.xls', 12: 'results1time12.xls', 18:'results1time18.xls', 
24: 'results1time24.xls', 30: 'results1time30.xls', 36: 'results1time36.xls', 48: 'results1time48.xls'}

timedata = {}

desiredtimepoints = [7,12,18,24]# add more desired timepoints, or replace with timefiles.keys() when things start working for bigger datasets

# 1. Parse data from files for each time point. Get IDs (using STRING to crossreference)
for t in desiredtimepoints:
	data = parsedata.read_data(timefiles[t])
	idlist = [x['Public Identifier'] for x in data]
	# below can take a LOT of time on the first run, but fast if ids were previously checked. Got ~2586 labels; to check that they were there took only 3.1 sec
	iddict = stringnetworks.update_ids(idlist)
	timedata[t] = [x for x in idlist if (x in iddict) & (len(iddict[x])>0)]
	print '# Expressed at Time '+str(t)+': '+str(len(timedata[t]))
	#parsedata.plot_data(data,'P-Value','Log2 FC') #not much of a correlation, not informative

# 2. HMM work
import hmm
print 'Running HMM...'
(testhmm, interactors) = hmm.run_til_convergence_or_maxruns(0,timedata) #if we fix convergence problems, increase maxruns

# slow: just under 10 min total for 2 time points, 36 min for 3 time points

# 3. Graph visualization

datafolder = os.path.join(os.path.dirname(os.getcwd()),'data')
import ppigraph
for time in interactors:
	G = ppigraph.adjacency_graph(interactors[time],outfilename=os.path.join(datafolder,"test"+str(time)+".png"))
	ppigraph.save_graph(G,"test"+str(time)+"graph")

# DON'T print the graph. Turns out printing a 2000-node graph is hard.

# 4. Compare with STRING-predicted interation partners
import evalroc
for time in interactors:
	confusion = evalroc.confusion_matrix(interactors[time], iddict)
	print confusion
print 'Done'
