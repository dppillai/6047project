import os
import xlrd
import matplotlib.pyplot as plt

datafolder = os.path.join(os.path.dirname(os.getcwd()),'data')
desiredcols = ['Host Factor ID','Entrez Gene ID', 'Public Identifier'] #Log2 FC and P-Value treated separately
# Read data from excel file in data folder. 
# TODO: Get relevant columns of data. 
def read_data(filename):
	xlsfile = os.path.join(datafolder, filename)
	with xlrd.open_workbook(xlsfile) as book:
		f = book.sheet_by_index(0)
		header = [h.encode('ascii','ignore') for h in f.row_values(0)]
		cols={}
		for h in range(len(header)):
			if header[h] in desiredcols:
				cols[h] = header[h]
			elif ('Log2 FC' in header[h]):
				cols[h] = 'Log2 FC'#header[h]
			elif ('Log10 FC' in header[h]):
				cols[h] = 'Log10 FC'#header[h]
			elif ('P-Value' in header[h]):
				cols[h] = 'P-Value'#header[h]
		data = []
		for n in range(1,f.nrows):
			datatoadd = {}
			for col in cols.keys():
				v=f.cell(n,col).value
				if type(v)==unicode:
					v=v.encode('ascii','ignore')
				datatoadd[cols[col]] = v
			# only include overexpressed
			if ('Log2 FC' in datatoadd) & (datatoadd['Log2 FC'] > 0):
				data.append(datatoadd)
		# equivalent of closing
		book.release_resources()
	return data#(data, cols)

def plot_data(data,col1,col2):
	datapairs=[]
	for d in data:
		datapairs.append((d[col1],d[col2]))
	datapairs = sorted(datapairs, key = lambda x: x[0])
	plt.plot([x[0] for x in datapairs],[x[1] for x in datapairs])
	plt.show()
