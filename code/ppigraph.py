import networkx as nx
import matplotlib.pyplot as plt
import cPickle

# graphs adjdict, saving to outfilename. 
def adjacency_graph(adjdict, outfilename="", show=False):
	if len(adjdict)>0:
		G = nx.Graph(adjdict)
	else:
		G = nx.barabasi_albert_graph(100,5)
	pos = nx.spring_layout(G) # positions for nodes
	print pos
	nodes = pos.keys()
	# Draw nodes and edges
	nx.draw_networkx_nodes(G, pos, nodelist = nodes, node_color='r', node_size=500, alpha=0.8)
	nx.draw_networkx_edges(G, pos, width=1.0, alpha=0.5, edge_color='b')
	# Label nodes
	nodedict = {}
	for i in range(len(nodes)):
		nodedict[nodes[i]] = r'$'+str(nodes[i])+'$'
	nx.draw_networkx_labels(G, pos, nodedict, font_size = 16)

	# Make plot
	plt.axis('off')
	if len(outfilename) > 0:
		plt.savefig(outfilename)
	if show:
		plt.show()
	return G

def subgraph_node(adjdict, node):
	pass

def save_graph(G,filename):
    with open(filename,'w') as f:
        cPickle.dump(G, f)
        f.close()

#testadjdict = {'Prot1':['Prot2','Prot3'],'Prot2':['Prot4']}
