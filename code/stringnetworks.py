import requests
import os
import cPickle
datafolder = os.path.join(os.path.dirname(os.getcwd()),'data')

idfile = os.path.join(os.getcwd(),'geneids')
# key is id in excel file (publicid), value is STRING-resolved official id

def save_ids(iddict):
    with open(idfile,'w') as f:
        cPickle.dump(iddict, f)
        f.close()

def load_ids():
    try:
        with open(idfile,'r') as f:
            iddict= cPickle.load(f)
            f.close()
    except IOError: #can happen if the file didn't exist before
        iddict = {}
    return iddict

def get_id(iddict, publicid):
	if publicid not in iddict:
		resolvedid = resolve_name(publicid)
		print resolvedid
		iddict[publicid] = resolvedid
		return (iddict, resolvedid)
	elif iddict[publicid] == publicid:
		resolvedid = resolve_name(publicid)
		print resolvedid
		if len(resolvedid) > 0:
			iddict[publicid] = resolvedid
		else:
			del iddict[publicid]
		return (iddict, resolvedid)
	elif len(iddict[publicid])<0:
		print publicid
	return (iddict, iddict[publicid])

def update_ids(pidlist):
	iddict = load_ids()
	for publicid in pidlist:
		(iddict, resolvedid) = get_id(iddict, publicid)
	save_ids(iddict)
	return iddict

#TODO expand these methods to include (optional?) custom params

# Get list of names of interactors from STRING, given identifier as argument (publicid)
# Note: first item in list is probably id of original interactor itself (I think... todo verify)
def get_interactor_names(publicid, score=500):
	r=requests.get("http://string-db.org/api/tsv-no-header/interactors?identifier="+publicid+'&required_score='+str(score))
	if r.status_code == 200:
		return r.content.split('\n')[1:-1] #deletes trailing blank space
	return []

def resolve_name(publicid):
	r = requests.get("http://string-db.org/api/tsv-no-header/resolve?identifier="+publicid+"&species=9606")
	if r.status_code == 200:
		return r.content.split('\t')[0]#first col is stringId
	return ''

def get_all_interactors(publicidlist):
	interactors = {}
	for pid in publicidlist:
		interactors[pid] = get_interactor_names(pid)
	return interactors

def resolved_list(ids):
	resolved = []
	for i in ids:
		resolved.append(resolve_name(i))
	return resolved

def predicted_observed(pred_interactors, observed_data):
	interactors = []
	for p in pred_interactors:
		if p in observed_data:
			interactors.append(p)
	return interactors

def total_predicted_and_observed(interactors, observed):
	if not observed:
		observed=interactors.keys()
	pred = 0
	predobs = 0
	for i in interactors:
		pred += len(interactors[i])
		predobs += len(predicted_observed(interactors[i],observed))
	return (pred, predobs)
# ID formatted as 9606.ENSP*. the 9606 refers to Homo sapiens taxa id. 
# in ENS (ensembl), the P refers to protein (T for transcript, G for gene)

# Save STRING network image to disk (data/networks), given identifier as argument (publicid)
def save_network_image(publicid):
	r = requests.get("http://string-db.org/api/image/network?identifier="+publicid+"&required_score=950&limit=10&network_flavor=evidence", stream=True)
	if r.status_code == 200:
		savepath = os.path.join(datafolder,'networks',publicid+'.png')
		with open(savepath,'wb') as f:
			for chunk in r.iter_content():
				f.write(chunk)
			f.close()
		return savepath
	return None

def images_all_ids(publicidlist):
	print len(publicidlist)
	for p in publicidlist:
		save_network_image(p)
